<?php
class Perusahaan_model extends CI_Model{
    
    function get_data(){
    	return $this->db->get('form_perusahaan')->result();
    }

    function get_data_perusahaan(){
        return $this->db->query('SELECT * FROM `form_perusahaan`, `produk` WHERE `form_perusahaan`.`type_produk` = `produk`.`uuid_produk`')->result();
    }

    function get_type_produk(){
    	return $this->db->get('type_produk')->result();
    }

    function get_bidang(){
    	return $this->db->get('jenis_bidang')->result();
    }

    function insert($data){
    	return $this->db->insert('form_perusahaan', $data);
    }

    function get_users(){
    	return $this->db->get('users')->result();
    }

    function get_kategori(){
        return $this->db->get('kategori')->result();
    }

    function get_produk(){
        return $this->db->get('produk')->result();
    }
}