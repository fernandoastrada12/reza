<?php
class Login_model extends CI_Model{

    public      $username;
    public      $password;
    public      $nik;
    public      $email;
    public      $no_hp;
    public      $level;
    public      $id;

    public function rules(){
        return [
            [
                'field' => 'username',
                'label' => 'username',
                'rules' => 'required'
            ],
            [
                'field' => 'password',
                'label' => 'password',
                'rules' => 'required'
            ],
            [
                'field' => 'level',
                'label' => 'level',
                'rules' => 'numeric'
            ],
            [
                'field' => 'email',
                'label' => 'email',
                'rules' => 'required'
            ],
            [
                'field' => 'no_hp',
                'label' => 'no_hp',
                'rules' => 'required'
            ],

        ];

    }

    //cek nip dan password dosen
    function cek_login($username, $password){
        $this->db->select("*");
        return $this->db->get_where('users', array('username'=>$username, 'password' => $password))->result();
    }
    function jumlah_visit(){
            return  $this->db->query("select SUM(jumlah_visit) as total from users")->result();
    }

    function get_users(){
        return $this->db->query("select * from users")->result();
    }

    function get_team(){
    	return $this->db->get('jenis_bidang')->result();

    }

    function get_level(){
    	return $this->db->get('level')->result();
    }
    function add_visit($usernme,$jumlah_visit,$users){
        $this->db->where('username', $username);
        $this->db->update($users, $jumlah_visit);
    }

    function get_nik(){
        return $this->db->get('reporting_teknisi')->result();
    }

    function add_users($data){

    	return $this->db->insert('users', $data);
    }

 	function delete_users($uuid){
 		$this->db->where('uuid', $uuid);
 		$this->db->delete('users');
 		}

    function change_password($username,$data,$users){
        $this->db->where('username', $username);
        $this->db->update($users,$data);
    }
}
