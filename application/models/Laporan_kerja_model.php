<?php
class Laporan_kerja_model extends CI_Model{

    function get_data(){
    	return $this->db->get('reporting_teknisi')->result();
    }

    function get_perusahaan(){
    	return $this->db->get('form_perusahaan')->result();
    }

        function get_perusahaan_satu($nama_perusahaan){
        	return	$this->db->query('SELECT * FROM `form_perusahaan`, `produk` WHERE `form_perusahaan`.`nama_perusahaan` LIKE "'.$nama_perusahaan.'"  AND `form_perusahaan`.`type_produk` = `produk`.`uuid_produk`')->result();

/*SELECT * FROM `form_perusahaan`, `produk` WHERE `form_perusahaan`.`nama_perusahaan` LIKE 'SINARMAS' AND `form_perusahaan`.`type_produk` = `produk`.`uuid_produk`*/
        //return $this->db->get_where('form_perusahaan', array('nama_perusahaan' => $nama_perusahaan))->result();
    }

    function insert($data){
        return $this->db->insert('reporting_teknisi', $data);
    }
    function update($data,$where,$table){
        $this->db->where($where);
        $this->db->update($table, $data);
    }

    //mencetak data user berdasarkan akses teknisi
    function get_user(){
        return $this->db->query('SELECT * FROM `users` where `level`= 3')->result();
    }

    function get_data_id($nama){
        return $this->db->query("SELECT * from reporting_teknisi, users WHERE users.username LIKE '$nama' and reporting_teknisi.engineer LIKE '%$nama%'")->result();
    }
    function get_preview($id_report){
        return $this->db->query('SELECT * FROM `reporting_teknisi` WHERE `reporting_teknisi`.`id_report`="'.$id_report.'"
')->result();
    }
    function Engineer($keyword){
      $this->db->SELECT('*');
      $this->db->from('reporting_teknisi');
        $this->db->like('engineer', $keyword);
        return $this->db->get()->result();
    }
}
