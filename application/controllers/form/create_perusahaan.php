<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Create_perusahaan extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->library('Uuid');
		$this->load->model('perusahaan_model');

	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		 /*print_r($this->session->all_userdata());*/
		$data['produk'] 		= $this->perusahaan_model->get_produk();
		$data['bidang'] 	= $this->perusahaan_model->get_bidang();
		
		$this->load->view('form/create_form_perusahaan', $data);
		
	}

	function insert(){

		$data['nama_perusahaan'] 	= $this->input->post('nama_perusahaan');
		$data['bidang'] 		= $this->input->post('bidang');
		$data['type_produk'] 	= $this->input->post('type');
		$data['status'] 		= $this->input->post('status');
		$data['user_created'] 		=$this->session->userdata('ses_nama');
		$data['uuid_perusahaan'] =  $this->uuid->v4();
		$form = $this->perusahaan_model->insert($data);
		$simpan['form'] = $this->perusahaan_model->get_data();
		redirect('form/create_perusahaan', $simpan);
	}
}
