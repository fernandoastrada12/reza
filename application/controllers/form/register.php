<?php
 defined('BASEPATH') OR exit('No direct script access allowed');
 
 class Register extends CI_Controller {
     
     function __construct(){
         parent::__construct();
         $this->load->model('login_model');
         $this->load->library('Uuid'); 
     }
 
     public function add_teknisi(){
        $data['nik']        = $this->input->post('nik');
        $data['username']   = $this->input->post('username');
        $data['level']      = $this->input->post('level');
        $data['password']   = md5($this->input->post('password'));
        $data['email']      = $this->input->post('email');
        $data['team']      = $this->input->post('team');
        $data['no_hp']   = $this->input->post('no_hp');
        
        $data['uuid']   = $this->uuid->v4();

        $this->login_model->add_users($data);/*
            echo $this->db->last_query();*/
        $data['users'] = $this->login_model->get_users();
        $this->load->view('admin/users', $data);
     }

     public function index() {
        
        $data['bidang'] = $this->login_model->get_team();
        $data['level']  = $this->login_model->get_level();
        $this->load->view('admin/nambah_users', $data);
     }

 }