<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Persetujuan extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->library('Uuid');
		$this->load->model('laporan_kerja_model');
		$this->load->model('perusahaan_model');


	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		 /*print_r($this->session->all_userdata());*/
		$data = array(
        	'log_file' => $this->input->get('log_file')
		);
		$where = array(
        	'id_report' => $this->input->get('id_report')
		);
		$report = $this->laporan_kerja_model->update($data,$where,'reporting_teknisi');

		 if ($this->session->userdata('akses')=='1' || ($this->session->userdata('akses') == '2'))  {
		 $data['data'] = $this->laporan_kerja_model->get_data();
		$this->load->view('form/view_laporan_kerja', $data);
		 	
		 }else if($this->session->userdata('akses')=='3'){
		 $nama =  $this->session->userdata('ses_nama');
		 $data['data']	= $this->laporan_kerja_model->get_data_id($nama);
		 $this->load->view('form/view_laporan_kerja', $data);	
		}

	}	
}
