<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Post_berita extends CI_Controller{

	function __construct(){
		parent::__construct();
		$this->load->library('upload');
		$this->load->model('berita_model');
		$this->load->helper('text');
	}
	public function index(){
		$this->load->view('teknisi/v_post_news');
	}

	public function simpan_post(){


			$data['berita_judul'] = $this->input->post('judul');
			$data['berita_isi'] = $this->input->post('berita');

			$simpan = $this->berita_model->insert($data);
			/*redirect('teknisi/form/post_berita/list');*/
			redirect('teknisi/form/post_berita');
				}
/*
        if(!empty($_FILES['filefoto']['name'])){
            if ($this->upload->do_upload('filefoto')){
                $gbr = $this->upload->data();
                //Compress Image
                $config['image_library']='gd2';
                $config['source_image']='./assets/images/'.$gbr['file_name'];
                $config['create_thumb']= FALSE;
                $config['maintain_ratio']= FALSE;
                $config['quality']= '60%';
                $config['width']= 710;
                $config['height']= 420;
                $config['new_image']= './assets/images/'.$gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
 
                $gambar=$gbr['file_name'];
                $jdl=$this->input->post('judul');
                $berita=$this->input->post('berita');
 
                $this->m_berita->simpan_berita($jdl,$berita,$gambar);
                redirect('post_berita/lists');
        }else{
            redirect('post_berita');
        }
                      
        }else{
            redirect('post_berita');
        }
*/
	public function list(){
		$data['data'] =$this->berita_model->get_berita_all();
		$this->load->view('teknisi/v_post_list', $data);
	}

 
	 function get_preview(){
		/*$id_berita = $this->berita_model->get_berita_id($this->input->get('berita_id'));*/
/*		echo $this->db->last_query();
*/		$data['data'] =$this->berita_model->get_berita_id($this->input->get('berita_id'));
	$this->load->view('teknisi/v_post_view', $data);
		}	
}
