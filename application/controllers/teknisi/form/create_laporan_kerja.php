<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Create_laporan_kerja extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->library('Uuid');
		$this->load->model('laporan_kerja_model');
		$this->load->model('perusahaan_model');


	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		 /*print_r($this->session->all_userdata());*/
		 $data['user']			= $this->perusahaan_model->get_users();
		 $data['nik']			= $this->laporan_kerja_model->get_user();

		$data['company'] 		= $this->laporan_kerja_model->get_perusahaan();
		$data['job_category']	= $this->perusahaan_model->get_kategori();

		/*$data['bidang'] 	= $this->perusahaan_model->get_bidang();*/
		
		$this->load->view('form/create_laporan_kerja', $data);
		
	}

	function get_perusahaan(){
		$company 		= $this->laporan_kerja_model->get_perusahaan_satu($this->input->get('nama_perusahaan'));
		echo json_encode($company);
	}

	function insert(){

		$data['nama_perusahaan'] 	= $this->input->post('nama_perusahaan');
		$data['alamat_perusahaan']	= $this->input->post('alamat_perusahaan');
		$data['pic'] 				= $this->input->post('pic');
		$data['no_hp']				= $this->input->post('no_hp');
		$data['nama_type_produk']	= $this->input->post('type');
		$data['uuid_type_produk'] 	= $this->input->post('type1');
		$data['uuid_produk']		= $this->input->post('produk');
		$data['serial_number']		= $this->input->post('sn');
		$data['versi']				= $this->input->post('versi');
		$data['kode_kategori'] 		= implode(', ', $this->input->post('job_category'));
		$data['job_sum']			= $this->input->post('job_summary');
		$data['job_desk']			= $this->input->post('deskripsi');
		$data['email']				= $this->input->post('customer');
		$data['date_visit']			= $this->input->post('date_visit');
		$data['engineer']				= implode(', ', $this->input->post('engineer'));
		$data['uuid_reporting'] 	= $this->uuid->v4();

		$report = $this->laporan_kerja_model->insert($data);

		redirect('teknisi/form/create_laporan_kerja', $report);
	}
}
