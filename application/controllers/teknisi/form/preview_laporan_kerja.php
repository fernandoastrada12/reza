<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Preview_laporan_kerja extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->library('Uuid');
		$this->load->model('laporan_kerja_model');
		$this->load->model('perusahaan_model');

	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		 /*print_r($this->session->all_userdata());*/
		

$data['id_report'] = $this->laporan_kerja_model->get_preview($this->input->get('id_report'));
$data['nik']	= $this->laporan_kerja_model->get_user();



		/*echo json_encode($data);*/

$data['job_category']	= $this->perusahaan_model->get_kategori();
	
		/*$data['bidang'] 	= $this->perusahaan_model->get_bidang();*/
		
		$this->load->view('form/preview_laporan_kerja', $data);
		
	}
}
