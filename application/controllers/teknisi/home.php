<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct(){
		parent::__construct();
		/*if ($this->session->userdata('akses')!== TRUE) {
			 $url=base_url();
            redirect($url);
		}*/
		$this->load->model('laporan_kerja_model');
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.phphp, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		//allowing akses to admin only
		if($this->session->userdata('akses')=='3'){
		 $nama =  $this->session->userdata('ses_nama');
		 $data['data']	= $this->laporan_kerja_model->get_data_id($nama);
		 $this->load->view('form/view_laporan_kerja', $data);
		 		}else{
			echo "Access Denied";
		}
	}
}
