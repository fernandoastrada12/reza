<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Change_password extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('login_model');
		$this->load->library('form_validation');
		$this->load->library('session');
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.phphp, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		//allowing akses to admin only
		$this->load->view('change_password');
	}

	function change(){
		$username = $this->session->userdata['ses_nama'];
		$data['password'] = md5($this->input->post('new_password'));

		$this->login_model->change_password($username, $data, 'users');
		$this->session->sess_destroy();
		redirect('login');
}
}
