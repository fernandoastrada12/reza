<?php
class Login extends CI_Controller{
    function __construct(){
        parent::__construct();
        $this->load->model('login_model');
    }

    function index(){
        $this->load->view('login');
    }

    function auth(){
        $username = $this->input->post('username');
        $password = $this->input->post('password');

        $user = $this->login_model->cek_login($username,md5($password));

        if(!empty($user)){
            if($user[0]->level =='1'){
                $this->session->set_userdata('akses','1');
                $this->session->set_userdata('ses_nama', $user[0]->username);
                redirect('admin/home');
            }
            elseif ($user[0]->level == '2') {
                $this->session->set_userdata('akses','2');
                $this->session->set_userdata('ses_nama',$user[0]->username);
                redirect('admin/home');
            }
            elseif ($user[0]->level == '3') {

                
                $this->session->set_userdata('akses','3');
                $this->session->set_userdata('ses_nama',$user[0]->username);
/*                $jmlh = 1;
                $user[0]->jumlah_visit = $user[0]->jumlah_visit + $jmlh; 
                $this->login_model->add_visit($user[0]->username, $user[0]->jumlah_visit, 'users');/*
/*                print_r($data['jumlah_visit']);*/
                redirect('teknisi/home');
            }
        }
        else { //jika username dan password tidak di temukan atau salah
            $url = base_url();
            echo $this->session->set_flashdata('msg','Username Atau Password Salah');
            redirect($url);
        }

      /*  $username=htmlspecialchars($this->input->post('username',TRUE),ENT_QUOTES);
        $password=htmlspecialchars($this->input->post('password',TRUE),ENT_QUOTES);

        $cek_admin=$this->login_model->cek_login($username,$password);

        var_dump(($cek_admin));*/
 /*
        if($cek_admin->num_rows() > 0){ //jika login sebagai SPV/admin
                $data=$cek_admin->row_array();
                $this->session->set_userdata('masuk',TRUE);
                 if($data['level']=='1'){ //Akses admin
                    $this->session->set_userdata('akses','1');
                    $this->session->set_userdata('ses_nama',$data['nama']);
                    redirect('page');

                 }else{ //akses SPV
                    $this->session->set_userdata('akses','2');
                    $this->session->set_userdata('ses_nama',$data['nama']);
                    redirect('page');
                 }

        }else{ //jika login sebagai teknisi
                    $cek_teknisi=$this->login_model->auth_teknisi($username,$password);
                    if($cek_teknisi->num_rows() > 0){
                            $data=$cek_teknisi->row_array();
                    $this->session->set_userdata('masuk',TRUE);
                            $this->session->set_userdata('akses','3');
                            $this->session->set_userdata('ses_nama',$data['nama']);
                            redirect('page');
                    }else{  // jika username dan password tidak ditemukan atau salah
                            $url=base_url();
                            echo $this->session->set_flashdata('msg','Username Atau Password Salah');
                            redirect($url);
                    }
        }*/

    }

    function logout(){
        $this->session->sess_destroy();
        $url=base_url('');
        redirect($url);
    }


}
