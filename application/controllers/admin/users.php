<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

	function __construct(){
		parent::__construct();
		/*if ($this->session->userdata('akses')!== TRUE) {
			 $url=base_url();
            redirect($url);
		}*/
		$this->load->model('login_model');
		$this->load->library('form_validation');
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.phphp, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		//allowing akses to admin only
		if($this->session->userdata('akses')=='1' || $this->session->userdata('akses')=='2'){
			$data['users'] = $this->login_model->get_users();
		$this->load->view('admin/users', $data);
		
		}else{
			echo "Access Denied";
		}
	}

	function users(){
		$users = $this->login_model->get_users();
		$this->load->view('admin/users', $users);
	}


}
