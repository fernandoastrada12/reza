<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

	function __construct(){
		parent::__construct();
		/*if ($this->session->userdata('akses')!== TRUE) {
			 $url=base_url();
            redirect($url);
		}*/
		$this->load->model('login_model');
		$this->load->library('Uuid');
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.phphp, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data['level'] = $this->login_model->get_level();
		$data['bidang'] = $this->login_model->get_team();
		$this->load->view('admin/nambah_users', $data);
	}


	function add_teknisi(){
		
		$data['username'] 	= $this->input->post('username');
		$data['nik'] 		= $this->input->post('nik');
		$data['password'] 	= md5($this->input->post('password'));
		$data['level'] 		= $this->input->post('level');
		$data['email'] 		= $this->input->post('email');
		$data['no_hp'] 		= $this->input->post('no_hp');
		$data['team']		= $this->input->post('team');
		$data['uuid'] =  $this->uuid->v4();

		$users = $this->login_model->add_users($data);
		$simpan['users'] = $this->login_model->get_users();
		$this->load->view('admin/users', $simpan);
	}

	function delete_user($uuid){
		$this->login_model->delete_users($uuid);
		redirect('admin/users');
	}
}
