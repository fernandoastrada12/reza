<!DOCTYPE html>
<html>
<?php $this->load->view('teknisi/head_teknisi') ?>
<body>

	<div class="wrapper">
	<?php $this->load->view('teknisi/header_teknisi') ?>
	<section class="content">
      <div class="row">
        <div class="col-lg-12">
          <div class="card-body">
          	<h1>Pos Berita</h1>
          	<?php foreach ($data as $key => $value) {?>
          		<div class="form-group row">
                	<label class="col-sm-2 col-form-label">Judul Berita</label>
                	<div class="col-sm-10">
                		<input type="text" value="<?php echo $value->berita_judul?>"  class="form-control" disabled>
                   	</div>
                </div>
                <div class="form-group row">
 				   	<label for="alamat" class="col-sm-2 col-form-label">Isi Knowledge</label>
    				<div class="col-sm-10">
      					<textarea disabled class="form-control" ><?php echo $value->berita_isi?></textarea>
    				</div>
  				</div>
  				<div class="form-group row">
  					<a href="<?php echo base_url().'index.php/teknisi/form/post_berita/list'?>">
  						BACK
  					</a>
  				</div>
<?php   	}?>
			</div>
 		 </div>

      </div>

    </section>


	</div>

</body>
</html>
