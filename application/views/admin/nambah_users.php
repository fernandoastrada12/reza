<!DOCTYPE html>
<html>
<?php $this->load->view('admin/head') ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

<?php $this->load->view('admin/header') ?>

  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url('assets/template/back/dist') ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Alexander Pierce</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">

        </div>
      </form>
      <!-- /.search form -->
     <?php $this->load->view('admin/sidebar') ?>
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-lg-12">
          <div class="card m-t-3">
            <div class="card-body">
              <a href="<?php echo base_url().'index.php/admin/users' ?>" class="btn btn-default btn-sm">Kembali</a>

             <form action="<?php echo base_url().'index.php/admin/register/add_teknisi'?>" method="post">
             <p><input type="text" name="nik" id="nik" class="form-control" placeholder="NIK" required>
            </p>
             <p><input type="text" name="username" id="username" class="form-control" placeholder="Username" required>
            </p>


            	<label>Level

            		<select class="form-control" name="level" id="level">
            			<?php foreach ($level as $key => $value) {?>
            				<option value="<?php echo $value->id_level;?>"><?php echo $value->nama_level;?>

            				</option>
            					<?php }
            			?>
            			</select>

            </label>
            </div>

            <p>
             	<input type="password" name="password" id="password" class="form-control" placeholder="Password" required>
            </p>
            <p>
            	<input type="email" name="email" id="email" placeholder="Email" required class="form-control">
            </p>
            <p>
            	<input type="text" name="no_hp" id="no_hp" placeholder="nomor hp " required class="form-control">
            </p>

            <!-- bidang -->
          <?php foreach ($bidang as $key => $value) { ?>
                  <div class="checkbox">
              <label><input type="checkbox" value="<?php echo $value->nama_bidang; ?>" name="team"> <?php echo $value->nama_bidang;?></label>
           </div>
          <?php }
            ?>
            <p>
            	<input type="submit" name="button" value="register">
              <input type="reset" name="reset">
            </p>
             </form>
            </div>
          </div>
        </div>
      </div>
    </section>
        </section>
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('admin/footer') ?>

</body>
</html>
