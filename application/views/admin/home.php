<!DOCTYPE html>
<html>
<?php $this->load->view('admin/head') ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

<?php $this->load->view('admin/header') ?>

  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url('assets/template/back/dist') ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $this->session->userdata('ses_nama')?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          </div>
      </form>
      <!-- /.search form -->
     <?php $this->load->view('admin/sidebar') ?>
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>PT. Daya Cipta Mandiri</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-lg-12">
          <div class="card m-t-3">
            <div class="card-body">
              <h4 class="text-black">Data Users</h4>
              <?php if ($this->session->userdata('akses') == '1') {?>
                 <a href="<?php echo base_url().'index.php/admin/register' ?>" class="btn btn-default btn-sm"> Nambah Teknisi</a>
              <?php }?>

             <table id="users" class="table table-bordered table-striped table-responsive">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>NIK</th>
                    <th>Nama</th>
                    <th>Team</th>
                    <th>Email</th>
                    <th>Nomor HP</th>
                    <th>Waktu Pendaftaran</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $i = 0;
                  foreach ($users as $data){
                  $i++;
                  ?>
                  <tr>
                    <td><?php echo $i?></td>
                    <td><?php echo $data->nik?></td>
                    <td><?php echo $data->username?></td>
                    <td><?php echo $data->team?></td>
                    <td><?php echo $data->email?></td>
                    <td><?php echo $data->no_hp?></td>
                    <td><?php echo $data->created_date?></td>
                    <!-- contoh session -->
                    <td><?php if ($this->session->userdata('akses') == '1'){ ?>   <a href="<?php echo base_url().'index.php/admin/register/delete_user/'.$data->uuid;?>" class="btn btn-danger">HAPUS</a><?php } ?>
                        <button type="button" class="btn btn-warning">EDIT</button></td>
                  </tr>
                  <?php }?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('admin/footer') ?>

</body>
</html>
