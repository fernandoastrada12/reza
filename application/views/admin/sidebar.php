 <!-- sidebar menu: : style can be found in sidebar.less -->
     <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        
        <!-- Akses sidebar untuk admin -->
        <?php if($this->session->userdata('akses') =='1' || ($this->session->userdata('akses') == '2')) { ?>
        <li>
            <a href="<?php echo base_url().'index.php/view_form_perusahaan' ?>"><i class="fa fa-book"></i> <span>Form Perusahaan</span></a></li>
          <!-- <ul class="treeview-menu">
            <li class="treeview">
              <a href="#"><i class="fa fa-circle-o"></i> Form Perusahaan
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li>
                  <a href="<?php echo base_url().'index.php/view_form_perusahaan'?>"><i class="fa fa-circle-o"></i> View Form Perusahaan
                    <span class="pull-right-container">
                    </span>
                  </a>
                </li>
                <li>
                  <?php if ($this->session->userdata('akses') =='1') { ?>
                    <a href="<?php echo base_url().'index.php/form/create_perusahaan'?>"><i class="fa fa-circle-o"></i> Create Form Perusahaan
                    <span class="pull-right-container">
                    </span>
                  </a>
                  <?php } ?> 
                </li>
              </ul> -->
            <!-- </li>
          </ul> -->
          <?php } ?>
          <li class="treeview">
            <a href="#"><i class="fa fa-book"></i> Laporan Kerja Teknisi
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li>
                <?php if($this->session->userdata('akses') == '3'){ ?>
                <a href="<?php echo base_url().'index.php/teknisi/form/create_laporan_kerja'?>"><i class="fa fa-circle-o"></i> Create Laporan Kerja
                  <span class="pull-right-container"></span>
                </a>
                <?php
                }?>
              </li>
              <li>
              <a href="<?php echo base_url().'index.php/teknisi/form/view_laporan_kerja'?>"><i class="fa fa-circle-o"></i> View Laporan Kerja
                  <span class="pull-right-container"></span>
                </a>
            </li>
            </ul>
          </li>

        <li>
          <?php if($this->session->userdata('akses') == '1' || ($this->session->userdata('akses') == '2' )){ ?>
            <a href="<?php echo base_url().'index.php/admin/users' ?>"><i class="fa fa-book"></i> <span>Users</span></a></li>
          <?php 
          } ?>

          <li class="treeview">
            <a href="#"><i class="fa fa-book"></i> Knowledge
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li>
                <?php if($this->session->userdata('akses') == '3'){ ?>
                <a href="<?php echo base_url().'index.php/teknisi/form/post_berita'?>"><i class="fa fa-circle-o"></i> Create Knowledge
                  <span class="pull-right-container"></span>
                </a>
                <?php
                }?>
              </li>
              <li>
              <a href="<?php echo base_url().'index.php/teknisi/form/post_berita/list'?>"><i class="fa fa-circle-o"></i> View Knowledge
                  <span class="pull-right-container"></span>
                </a>
            </li>
            </ul>
          </li>

      </ul>
    </section>
    <!-- /.sidebar -->