<!DOCTYPE html>
<html>
<?php $this->load->view('teknisi/head_teknisi') ?>
<body>

	<div class="wrapper">
	<?php $this->load->view('teknisi/header_teknisi') ?>
	<section class="content">
      <div class="row">
        <div class="col-lg-12">
          <div class="card-body">
          	<h1>Customer</h1>

          	<?php foreach ($id_report as $key => $value) {?>
          		<div class="form-group row">
                	<label class="col-sm-2 col-form-label">Company</label>
                	<div class="col-sm-10">
                		<input type="text" value="<?php echo $value->nama_perusahaan?>"  class="form-control" disabled>
                   	</div>
                </div>
                <div class="form-group row">
 				   	<label for="alamat" class="col-sm-2 col-form-label">Alamat</label>
    				<div class="col-sm-10">
      					<textarea disabled class="form-control" ><?php echo $value->alamat_perusahaan?></textarea>
    				</div>
  				</div>
  				<div class="form-group row">
    					<label for="alamat" class="col-sm-2 col-form-label">Contact Person</label>
    				<div class="col-sm-10">
    					<input type="text" name="pic" id="pic" value="<?php echo $value->pic?>" class="form-control"  disabled>
    				</div>
  				</div>
      			<div class="form-group row">
    					<label for="alamat" class="col-sm-2 col-form-label">Telepon</label>
    				<div class="col-sm-10">
      					<input type="text" name="no_hp" id="no_hp" class="form-control" value="<?php echo $value->no_hp?>" disabled>
    				</div>
  				</div>
  				<div class="form-group row">
  						<label for="alamat" class="col-sm-2 col-form-label">Produk</label>
  				<div class="col-xs-3">
      					<input type="text" name="produk" id="produk" value="<?php echo $value->uuid_produk?>" class="form-control" disabled>
    		  	</div>
    					<label for="alamat" class="col-sm-2 col-form-label">Serial Number</label>
  					<div class="col-xs-4 col-sm-5">
				  		<input type="text" name="sn" id="sn" value="<?php echo $value->serial_number?>" class="form-control"  disabled>
  					</div>
  				</div>
				<div class="form-group row">
  						<label for="type" class="col-sm-2 col-form-label">Type</label>
  					<div class="col-xs-3">
      					<input type="text" id="type" name="type" class="form-control" value="<?php echo $value->nama_type_produk?>" disabled>
    		  		</div>
    					<label for="versi" class="col-sm-2 col-form-label">Versi</label>
  					<div class="col-xs-4 col-sm-5">
  						<input type="text" name="versi" id="versi" value="<?php echo $value->versi?>" class="form-control" disabled >
  					</div>
  	  			</div>
  	  			<div class="form-group row ">
  	  				<label for="job_category" class=" col-sm-2 col-form-label">Job Category</label>
  	  				<div class="checkbox col-sm-12">
  	  			<?php foreach ($job_category as $key => $data) { ?>
 					<div class="col-xs-3">
 					<input type="checkbox" disabled value="$nama->nama_kategori" <?php echo $value->kode_kategori == $data->nama_kategori ? 'checked' : ''; ?>><?php echo $data->nama_kategori?></input>
			<!-- <?php echo $value->kode_kategori.' ||   '.$data->nama_kategori.'<br/>';?> -->
 				</div>
    <?php          	} ?>
    				</div>
  	  				</div>

	<div class="form-group row">
    	<label class="col-sm-2 col-form-label"><br>Job Summary</label>
    		<div class="col-sm-10">
    			<br>
      			<textarea class="form-control" id="job_summary" name="job_summary" placeholder="Job Summary" disabled><?php echo $value->job_sum?> </textarea>
    		</div>
  </div>

  <div class="form-group row">
    	<label class="col-sm-2 col-form-label"><br>Job Description</label>
    		<div class="col-sm-10">
    			<br>
      			<textarea class="form-control" id="deskripsi" name="deskripsi" placeholder="Job Description" disabled><?php echo $value->job_desk?></textarea>
    		</div>
  </div>

  <div class="form-group row">
		 <label for="alamat" class="col-sm-2 col-form-label"><br>Customer Representative</label>
		  <div class="col-xs-3">
      			<br>
      		<input type="text" name="customer" id="customer" value="<?php echo $value->email?>" class="form-control" disabled>
 		</div>
 		<div class="col-sm-3">
 			<br>
 			<label class="col-form-label col-sm-3">DATE</label>
 			<div class="input-group date">
 			<input type="date" name="date_visit" class="form-control">
 			<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
 			</div>

 			<br>
 			<label class="col-xs-3 col-form-label">Engineer</label>
 			<?php $data = explode(", ", $value->engineer); ?>
 			<?php $i=0;?>


 			<?php foreach ($nik as $keys => $list) { ?>
 				<div class="col-xs-3">
 					<input type="checkbox" disabled value="$nama->nama_kategori" <?php echo $data[$i] == $list->username ? 'checked' : ''; ?>><?php echo $list->username?></input>
 				</div>

 				<?php $i++;	}?>
 		</div>
  </div>
<?php   	}?>

<!--
<?php $data = explode(", ", $value->engineer); ?>
 			<?php $i=0;?>


 			<?php foreach ($nik as $keys => $list) { ?>
 				<div class="col-xs-3">
 					<input type="checkbox" <?php echo $data[$i] == $list->username ? 'checked' : ''; ?>><?php echo $list->username;?>
 					</input>
 				</div>

 				<?php $i++;	}?>
 			 -->
<!-- <div class="form-group">
	<div class="col-sm-2">
    	<label for="alamat" class="col-form-label"><br>Customer Representative</label></div>
    		<div class="col-xs-3">
      			<br>
      			<input type="text" name="customer" id="customer" class="form-control" required>
    		</div>
    		<div class="form-group">
    			<div class="col-sm-3">
    				<br>
    	<label for="alamat" class="col-sm-3 col-form-label">DATE </label>
       		  <br>
              <label class="col-xs-3 col-form-label ">Engineer</label> <div class="col-sm-8">

<!--               <?php foreach ($nik as $key => $value) { ?>
                <div class="checkbox">
                  <label><input type="checkbox" name="engineer[]" value="<?php echo $value->username; ?>"> <?php echo $value->username; ?> </label>
              </div>
              <?php } ?>
 -->
 <a href="<?php echo base_url().'index.php/teknisi/form/view_laporan_kerja' ?>" class="btn btn-default btn-sm">Kembali</a>
            </form>
          </div>
        </div>

      </div>

    </section>


	</div>

</body>
</html>
