<!DOCTYPE html>
<html>
<?php $this->load->view('admin/head') ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

<?php $this->load->view('admin/header') ?>

  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url('assets/template/back/dist') ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $this->session->userdata('ses_nama')?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          </div>
      </form>
      <!-- /.search form -->
     <?php $this->load->view('admin/sidebar') ?>
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        FORM
        <small>Perusahaan</small>
      </h1>
      <ol class="breadcrumb">
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-lg-12">
          <div class="card-body">
            <a href="<?php echo base_url().'index.php/view_form_perusahaan' ?>" class="btn btn-default btn-sm">Kembali</a>

            <form action="<?php echo base_url().'index.php/form/create_perusahaan/insert'?>" method="post">
              <p>
                <p><input type="text" name="nama_perusahaan" id="nama_perusahaan" class="form-control" placeholder="nama_perusahaan" required>
            </p>
              </p>
              <p>
                <label>Type :
                <select class="form-control" name="type" id="type">
                  <option></option>
                  <?php foreach ($produk as $key => $value) {?>
                    <option value="<?php echo $value->uuid_produk;?>"><?php echo $value->nama_produk;?>

                    </option>
                      <?php }
                  ?>
                  </select>
                  </label>
              </p>
              <p>
                <label>
                  Bidang
                  <select class="form-control" name="bidang" id="bidang">
                    <option></option>
                    <?php foreach   ($bidang as $key => $value) { ?>
                      <option value="<?php echo $value->nama_bidang;?>">
                        <?php echo $value->nama_bidang;?>
                      </option>

                    <?php }?>

                  </select>
                </label>
              </p>
              <p>
              <label>Status </label>
              <select class="form-control" name="status" id="status">
                <option></option>
                <option>on progress</option>
                <option>sudah PO</option>
              </select>
              </p>
                <p>
                <input type="submit" name="button" value="Daftar">
                <input type="reset" name="reset">
              </p>
            </form>
          </div>
        </div>

      </div>

    </section>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('admin/footer') ?>

</body>
</html>
