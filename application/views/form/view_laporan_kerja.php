<!DOCTYPE html>
<html>
<?php $this->load->view('admin/head') ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">


<?php $this->load->view('admin/header') ?>

  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url('assets/template/back/dist') ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $this->session->userdata('ses_nama')?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          </div>
      </form>
      <!-- /.search form -->
     <?php $this->load->view('admin/sidebar') ?>
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        View
        <small>Laporan Kerja</small>
      </h1>
      <ol class="breadcrumb">
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    	<div class="container" style="margin-top: 40px;">
    		<div class="row">
    		<div class="col-lg-12">
    			<div class="row">
            <div class="cool">

            <?php echo form_open('teknisi/form/view_laporan_kerja/Engineer'); ?>
            <input type="text" name="keyword" placeholder="masuk nama">
              <input type="submit" name="cari" value="Cari">
                <?php echo form_close()?>


    				<div class="col-md-12">
    					<table class="table table-bordered table-striped table-responsive">
    					<thead>
    					<tr>
    						<th>no</th>
    						<th>Engineer</th>
    						<th>Company</th>
    						<th>Produk</th>
    						<th>Type</th>
    						<th>Tanggal visit</th>
                <th>Aksi</th>
                <th>Status</th>
                <!-- <th rowspan="2" ><center>Status</center></th> -->
    					</tr>
    					</thead>
    					<tr>
    						<tbody>
    							<?php
    							$i = 0;
    							foreach ($data as $value) {
    								$i++;?>
    								<tr>
    									<td><?php echo $i?></td>
    									<td><?php echo $value->engineer?></td>
    									<td><?php echo $value->nama_perusahaan?></td>
    									<td><?php echo $value->uuid_produk?></td>
                      <td><?php echo $value->nama_type_produk?></td>
                      <td><?php echo $value->date_visit?></td>
                      <td><a href="<?php echo base_url().'index.php/teknisi/form/preview_laporan_kerja/?id_report='.$value->id_report?>" class="btn btn-danger">View</a></td>

                      <?php if(!empty($value->log_file)){ ?>
                        <?php if($this->session->userdata('akses') == '2'){ ?>
                          <td><a href="<?php echo base_url().'index.php/teknisi/form/persetujuan/?id_report='.$value->id_report.'&log_file=Approval'?>" class="btn btn-danger" disabled>Approval</a></td>
                          <td><a href="<?php echo base_url().'index.php/teknisi/form/persetujuan/?id_report='.$value->id_report.'&log_file=Reject'?>" class="btn btn-danger" disabled>Reject</a></td>
                        <?php }elseif ($this->session->userdata('akses') == '3' || ($this->session->userdata('akses') == '1')) {?>
                        <td><label><?php echo $value->log_file?></label></td>
                      <?php } ?>

                      <?php }elseif (empty($value->log_file)) {?>
                       <?php if($this->session->userdata('akses') == '2'){ ?>
                          <td><a href="<?php echo base_url().'index.php/teknisi/form/persetujuan/?id_report='.$value->id_report.'&log_file=Approval'?>" class="btn btn-danger" >Approval</a></td>
                          <td><a href="<?php echo base_url().'index.php/teknisi/form/persetujuan/?id_report='.$value->id_report.'&log_file=Reject'?>" class="btn btn-danger">Reject</a></td>
                        <?php }elseif ($this->session->userdata('akses') == '3' || ($this->session->userdata('akses') == '1')) {?>
                         <td><label>belom terkonfirmasi</label></td>
                      <?php } ?>
                      <?php } ?>
    								</tr>
    							<?php } ?>
    						</tbody>
    					</tr>
    					</table>
    				</div>
<?php echo $this->pagination->create_links(); ?>
    		</div>
    		</div>
    	</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('admin/footer') ?>

</body>
</html>
