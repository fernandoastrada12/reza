<!DOCTYPE html>
<html>
<?php $this->load->view('admin/head') ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

<?php $this->load->view('admin/header') ?>

  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url('assets/template/back/dist') ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $this->session->userdata('ses_nama')?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          </div>
      </form>
      <!-- /.search form -->
     <?php $this->load->view('admin/sidebar') ?>
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        FORM
        <small>Kerja</small>
      </h1>
      <ol class="breadcrumb">
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-lg-12">
          <div class="card-body">
          	<h1>Customer</h1>
            <form action="<?php echo base_url().'index.php/teknisi/form/create_laporan_kerja/insert'?>" method="post">
                <div class="form-group row">
                	<label class="col-sm-2 col-form-label">Company</label>
                	<div class="col-sm-10">
                		<select class="form-control" name="nama_perusahaan" id="nama_perusahaan">
                			<option></option>
                			<?php foreach ($company as $key => $value) {?>
                				<option value="<?php echo $value->nama_perusahaan;?>">
                					<?php echo $value->nama_perusahaan;?>
                				</option>
                				<?php
                			} ?>
                		</select>
                   	</div>
                </div>
    <div class="form-group row">
    	<label for="alamat" class="col-sm-2 col-form-label">Alamat</label>
    		<div class="col-sm-10">
      			<textarea class="form-control" id="alamat_perusahaan" name="alamat_perusahaan" placeholder="Alamat Perusahaan" required></textarea>
    		</div>
  </div>

    <div class="form-group row">
    	<label for="alamat" class="col-sm-2 col-form-label">Contact Person</label>
    		<div class="col-sm-10">
    			<input type="text" name="pic" id="pic" class="form-control" placeholder="Contact Person" required>
    		</div>
  </div>

      <div class="form-group row">
    	<label for="alamat" class="col-sm-2 col-form-label">Telepon</label>
    		<div class="col-sm-10">
      			<input type="text" name="no_hp" id="no_hp" class="form-control" placeholder="+62 " required>
    		</div>
  </div>

  <div class="form-group row">
  		<label for="alamat" class="col-sm-2 col-form-label">Produk</label>
  	<div class="col-xs-3">
      			<input type="text" name="produk" id="produk" class="form-control" required>
    		  	</div>
    <label for="alamat" class="col-sm-2 col-form-label">Serial Number</label>
  	<div class="col-xs-4 col-sm-5">

  		<input type="text" name="sn" id="sn" class="form-control"  required>
  	</div>
  </div>

   <div class="form-group row">
  		<label for="type" class="col-sm-2 col-form-label">Type</label>
  	<div class="col-xs-3">
      			<input type="text" id="type" name="type" class="form-control" required>
            <input type="hidden" name="type1" id="type1">
    		  	</div>
    <label for="versi" class="col-sm-2 col-form-label">Versi</label>
  	<div class="col-xs-4 col-sm-5">

  		<input type="text" name="versi" id="versi" class="form-control"  >
  	</div>
  	  </div>


	<label for="job_category" class="col-form-label">Job Category</label>


		<div class="form-group checkbox">
   	<?php foreach ($job_category as $key => $value) { ?>
   	<div class="col-xs-3">

      	<input type="checkbox" name="job_category[]" value="<?php echo $value->nama_kategori; ?>"> <?php echo $value->nama_kategori;?>
    </div>
</div>

<?php } ?>


	<div class="form-group">
    	<label class="col-sm-2 col-form-label"><br>Job Summary</label>
    		<div class="col-sm-10">
    			<br>
      			<textarea class="form-control" id="job_summary" name="job_summary" placeholder="Job Summary" required></textarea>
    		</div>
  </div>

<div class="form-group">
    	<label class="col-sm-2 col-form-label"><br>Job Description</label>
    		<div class="col-sm-10">
    			<br>
      			<textarea class="form-control" id="deskripsi" name="deskripsi" placeholder="Job Description" required></textarea>
    		</div>
  </div>

<div class="form-group">
	<div class="col-sm-2">
    	<label for="alamat" class="col-form-label"><br>Customer Representative</label></div>
    		<div class="col-xs-3">
      			<br>
      			<input type="text" name="customer" id="customer" class="form-control" required>
    		</div>
    		<div class="form-group">
    			<div class="col-sm-3">
    				<br>
    				<label class="col-sm-3 col-form-label">DATE </label>
       					<div class="input-group date" >
    						<input type="DATE" class="form-control" name="date_visit" />
    						<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>

    					</div>
              <br>
              <label class="col-xs-3 col-form-label ">Engineer</label> <div class="col-sm-8">

              <?php foreach ($nik as $key => $value) { ?>
                <div class="checkbox">
                  <label><input type="checkbox" name="engineer[]" value="<?php echo $value->username; ?>"> <?php echo $value->username; ?> </label>
              </div>
              <?php } ?>
              </div>
    		<input type="submit" name="button" value="BUAT">
        	</div>

        </div>

		  </div>


            </form>
          </div>
        </div>

      </div>

    </section>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('teknisi/footer_teknisi') ?>

</body>
</html>
