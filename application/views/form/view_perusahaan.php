<!DOCTYPE html>
<html>
<?php $this->load->view('admin/head') ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

<?php $this->load->view('admin/header') ?>

  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url('assets/template/back/dist') ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $this->session->userdata('ses_nama')?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">

        </div>
      </form>
      <!-- /.search form -->
     <?php $this->load->view('admin/sidebar') ?>
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        view
        <small>List Customer</small>
      </h1>
      <ol class="breadcrumb">
      </ol>
    </section>

    <!-- Main content -->
    <br> </br>
    <a href="<?php echo base_url().'index.php/form/create_perusahaan' ?>" class="btn btn-default btn-sm">Nambah perusahaan</a>
     <table class="table table-bordered table-striped table-responsive" >
  <thead>
    <tr>
      <th scope="col">No</th>
      <th scope="col">Perusahaan</th>
      <th scope="col">Type</th>
      <th scope="col">Bidang</th>
      <th scope="col">Status</th>
      <th scope="col">Tanggal Register</th>
    </tr>
  </thead>
  <tbody>
    <?php
    $i=0;
    foreach ($data as $key => $value){
      $i++;
      ?>
    <tr>
      <th scope="row"><?php echo $i?></th>
      <td><?php echo $value->nama_perusahaan?></td>
      <td><?php echo $value->nama_produk?></td>
      <td><?php echo $value->bidang?></td>
      <td><?php echo $value->status?></td>
      <td><?php echo $value->created_date?></td>
    </tr>
  <?php } ?>
  </tbody>
</table>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('admin/footer') ?>

</body>
</html>
